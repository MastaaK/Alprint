# Get image prebuilt from docker library
FROM resin/raspberrypi3-alpine
RUN [ "cross-build-start" ]

RUN addgroup -S user -g 3000
RUN adduser -S user user -u 3000
RUN addgroup user tty
RUN addgroup user dialout
RUN apk update && \
  apk add --no-cache python yaml-dev py-setuptools && \
  apk add --no-cache --virtual .build &&\
  apk add git linux-headers gcc libc-dev python-dev py-setuptools python2 py-pip
RUN git clone https://github.com/foosel/OctoPrint.git /home/user/octoprint && \
  cd /home/user/octoprint && \
  pip install --upgrade pip && \
  pip install -r requirements.txt --no-cache-dir && \
  python setup.py install
RUN mkdir /home/user/.octoprint
RUN chown -R user:user /home/user/.octoprint
RUN apk del .build
RUN rm -rf /home/user/octoprint /root/.cache /var/cache/apk/* $0

RUN [ "cross-build-end" ] 